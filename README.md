# Modeling Female Meiosis

This repository contains codes and data for the paper "Mathematical modeling of human oocyte aneuploidy".
&nbsp;
&nbsp;

codes/
- Codes to process original data and perform associated analysis as discussed in the main text.
- Files ending with ".nb" should be opened with Mathematica.
- Files ending with ".R" are R files.

data/
- Original data parsed into various formats that were used to plot figures presented in the paper.
- README.md provides details on how each file was produced.

figures/
- Codes that can be used to reproduce the main figures in the paper.

supplementary_figures/
- Codes to reproduce all figures presented in the supplementary material.
