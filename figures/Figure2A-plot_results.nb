(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     31935,        717]
NotebookOptionsPosition[     30379,        684]
NotebookOutlinePosition[     30717,        699]
CellTagsIndexPosition[     30674,        696]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}]], "Input",
 CellLabel->
  "In[209]:=",ExpressionUUID->"88f33434-d7de-4e97-a359-8fae94630853"],

Cell[BoxData[
 RowBox[{"SetDirectory", "[", "\"\</pathTo/ModelingFemaleMeiosis\>\"", 
  "]"}]], "Input",
 CellChangeTimes->{{3.783022771793333*^9, 3.783022818620757*^9}, {
  3.783022886984514*^9, 
  3.783022915435347*^9}},ExpressionUUID->"1e7522db-f960-4479-b4f0-\
5ba4f7b1ba59"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"day5Data", "=", 
   RowBox[{"Import", "[", 
    RowBox[{
    "\"\<data/day5_proportions_rawPerIndv.csv\>\"", ",", " ", "\"\<CSV\>\""}],
     "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"day5Data", "[", 
  RowBox[{"[", "1", "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"day5Data", "=", 
   RowBox[{"DeleteCases", "[", 
    RowBox[{"day5Data", ",", 
     RowBox[{"{", "__String", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"-", "2"}], "}"}]}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.76277368088342*^9, 3.7627736880818567`*^9}, {
   3.762775095497695*^9, 3.762775103993043*^9}, {3.763320698346262*^9, 
   3.763320706434023*^9}, 3.771344775559751*^9, {3.7793838000896597`*^9, 
   3.779383836718433*^9}, {3.779383925570751*^9, 3.7793839451679993`*^9}, {
   3.7793842322296057`*^9, 3.779384234255905*^9}, 3.783022686657103*^9, 
   3.783022735695175*^9, 3.783022828243161*^9},
 CellLabel->
  "In[214]:=",ExpressionUUID->"db33ecc3-73dd-45e6-ba43-07a155814ec3"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"noError\"\>", ",", "\<\"oneError\"\>", 
   ",", "\<\"twoErrors\"\>", ",", "\<\"complex\"\>", ",", "\<\"trisomy\"\>", 
   ",", "\<\"monosomy\"\>", ",", "\<\"doubletrisomy\"\>", 
   ",", "\<\"doublemonosomy\"\>", ",", "\<\"trisomymonosomy\"\>"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.762775096249161*^9, 3.7627751076327467`*^9}, 
   3.763229552110952*^9, 3.7632308914418097`*^9, 3.763320717322575*^9, 
   3.771344784617319*^9, 3.779383951435004*^9, 3.783022650337109*^9, 
   3.783022687181243*^9, {3.7830228220059023`*^9, 3.7830228292727957`*^9}},
 CellLabel->
  "Out[215]=",ExpressionUUID->"2275eacb-85e4-4cc6-a5a2-2a8ba105b721"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"dt", "=", 
  RowBox[{"day5Data", "[", 
   RowBox[{"[", "1", "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.763229700113677*^9, 3.763229787443695*^9}, {
  3.76332074253222*^9, 3.763320768239996*^9}, {3.7633208480972757`*^9, 
  3.763320877480418*^9}, {3.771344782583755*^9, 3.7713448058305197`*^9}},
 CellLabel->
  "In[233]:=",ExpressionUUID->"381ad6e2-9548-48ba-9741-c9f12196d5d4"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.647216993815542`", ",", "0.227390875683427`", ",", "0.0688357085237967`",
    ",", "0.056556421977234`", ",", "0.114098772071345`", ",", 
   "0.113292103612082`", ",", "0.0185533745630546`", ",", 
   "0.0194496728511249`", ",", "0.0308326611096173`"}], "}"}]], "Output",
 CellChangeTimes->{{3.763229696321457*^9, 3.763229712032337*^9}, {
   3.763229779325514*^9, 3.763229787952825*^9}, 3.7633207693031893`*^9, {
   3.7633208709698257`*^9, 3.763320879029098*^9}, 3.7713448067181063`*^9, 
   3.779383972910081*^9, 3.783022861621306*^9},
 CellLabel->
  "Out[233]=",ExpressionUUID->"b37d9c85-3af1-4e3d-9128-038a71680567"]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{"m1", " ", "=", " ", 
   RowBox[{"Import", "[", 
    RowBox[{
    "\"\<data/Figure2A/model1_estimates_23q_global.txt\>\"", ",", " ", 
     "\"\<List\>\""}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m2", " ", "=", " ", 
   RowBox[{"Import", "[", 
    RowBox[{
    "\"\<data/Figure2A/model2_estimates_23q_23qstar_global.txt\>\"", ",", 
     " ", "\"\<List\>\""}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m3", " ", "=", " ", 
   RowBox[{"Import", "[", 
    RowBox[{
    "\"\<data/Figure2A/model3_estimates_23q_23qstar_23qt_global.txt\>\"", ",",
      " ", "\"\<List\>\""}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m4", "=", " ", 
   RowBox[{"Import", "[", 
    RowBox[{
    "\"\<data/Figure2A/model4_estimates_RS_23q_23qstar_global-free.txt\>\"", 
     ",", " ", "\"\<List\>\""}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7627721367815533`*^9, 3.7627722371738863`*^9}, {
  3.762772666482113*^9, 3.76277267642353*^9}, {3.762772735929029*^9, 
  3.762772746305922*^9}, {3.762773001914831*^9, 3.762773009289383*^9}, {
  3.762774162497046*^9, 3.762774172349103*^9}, {3.76277422126017*^9, 
  3.762774243294751*^9}, {3.7632295405355*^9, 3.763229544862336*^9}, {
  3.763229589896747*^9, 3.7632296242647867`*^9}, {3.7713446274876003`*^9, 
  3.7713446289850693`*^9}, {3.771344822435334*^9, 3.77134489560785*^9}, {
  3.779383895752536*^9, 3.779383905753935*^9}, {3.7793841240522137`*^9, 
  3.779384133496883*^9}, {3.779384247993155*^9, 3.779384264449872*^9}, {
  3.783022629223242*^9, 3.78302264352919*^9}, {3.783022706549803*^9, 
  3.783022707737495*^9}, {3.7830228331494427`*^9, 3.7830228497128468`*^9}},
 CellLabel->
  "In[225]:=",ExpressionUUID->"138aa53b-110b-47b4-927d-6ae585ee10f1"],

Cell[BoxData[
 RowBox[{"(*", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"https", ":"}], "//", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"mathematica", ".", "stackexchange", ".", "com"}], "/", 
       "questions"}], "/", "125149"}], "/", "how"}], "-", "do", "-", "i", "-",
     "read", "-", "in", "-", "and", "-", "plot", "-", 
    RowBox[{"3", "d"}], "-", 
    RowBox[{
     RowBox[{"data", "/", "125160"}], "#125160"}]}]}], "\[IndentingNewLine]", 
  "*)"}]], "Input",
 CellChangeTimes->{{3.762773135387501*^9, 3.762773137840805*^9}},
 CellLabel->
  "In[229]:=",ExpressionUUID->"1dbecdc4-4c27-4b65-b766-c979ffc9aae1"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"data", "=", 
   RowBox[{"Transpose", "[", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{"ToExpression", "[", 
        RowBox[{"ToString", "[", " ", 
         RowBox[{"m1", "[", 
          RowBox[{"[", "1", "]"}], "]"}], " ", "]"}], "]"}], "-", "dt"}], ",", 
      RowBox[{
       RowBox[{"ToExpression", "[", 
        RowBox[{"ToString", "[", " ", 
         RowBox[{"m2", "[", 
          RowBox[{"[", "1", "]"}], "]"}], " ", "]"}], "]"}], "-", "dt"}], ",",
       "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"ToExpression", "[", 
        RowBox[{"ToString", "[", " ", 
         RowBox[{"m3", "[", 
          RowBox[{"[", "1", "]"}], "]"}], " ", "]"}], "]"}], "-", "dt"}], ",",
       " ", 
      RowBox[{
       RowBox[{"ToExpression", "[", 
        RowBox[{"ToString", "[", " ", 
         RowBox[{"m4", "[", 
          RowBox[{"[", "1", "]"}], "]"}], " ", "]"}], "]"}], "-", "dt"}]}], 
     " ", "}"}], "]"}]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"yticks", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "\"\<no errors\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"2", ",", "\"\<one error\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"3", ",", " ", "\"\<two errors\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", "\"\<complex\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"5", ",", " ", "\"\<trisomy\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"6", ",", "\"\<monosomy\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"7", ",", "\"\<double trisomy\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"8", ",", "\"\<double monosomy\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"9", ",", "\"\<trisomy+monosomy\>\""}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"plt", "=", 
   RowBox[{"ArrayPlot", "[", 
    RowBox[{"data", ",", " ", 
     RowBox[{"PlotLegends", "\[Rule]", "Automatic"}], ",", 
     RowBox[{"FrameTicks", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"yticks", ",", "None"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"None", ",", "None"}], "}"}]}], "}"}]}]}], "]"}]}], 
  "  "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"ColorFunction", "\[Rule]", "GrayLevel"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.762773266332891*^9, 3.762773291790634*^9}, {
   3.762773324669056*^9, 3.7627733718204412`*^9}, {3.7627734214888573`*^9, 
   3.762773425144656*^9}, {3.762773701583097*^9, 3.762773730720521*^9}, {
   3.762774014726194*^9, 3.7627740167301693`*^9}, {3.762774047785234*^9, 
   3.762774123460392*^9}, {3.762774181685432*^9, 3.762774181989472*^9}, {
   3.762774380661045*^9, 3.762774388518628*^9}, {3.762774593957962*^9, 
   3.7627745945564957`*^9}, {3.762774885669983*^9, 3.762774886108551*^9}, {
   3.762774921042219*^9, 3.762774945943366*^9}, 3.763229666933874*^9, 
   3.763229798928018*^9, {3.763230006336795*^9, 3.7632300763432903`*^9}, {
   3.7632301196751213`*^9, 3.763230173022635*^9}, {3.763320887195909*^9, 
   3.763320887875092*^9}, 3.7713449097995043`*^9, {3.783022717813653*^9, 
   3.7830227207380533`*^9}},
 CellLabel->
  "In[234]:=",ExpressionUUID->"394fb833-604e-456c-8a85-452e141656f3"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "0.012397481135483668`", ",", "0.00019399021298294183`", ",", 
     "0.00019398976378992572`", ",", "0.00019400994280860395`"}], "}"}], ",", 
   
   RowBox[{"{", 
    RowBox[{
    "0.019363251698610667`", ",", "0.000129328734822437`", ",", 
     "0.0001293301227664745`", ",", "0.00012932802903342644`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "0.04678176771388303`"}], ",", 
     RowBox[{"-", "0.0005173104742641071`"}], ",", 
     RowBox[{"-", "0.0005173122321963097`"}], ",", 
     RowBox[{"-", "0.0005173250065791113`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "0.015021034879789091`", ",", "0.00019399152645905443`", ",", 
     "0.00019399234564036055`", ",", "0.0001939870347373307`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0.00915298513779933`", ",", 
     RowBox[{"-", "0.0007268425784108246`"}], ",", 
     RowBox[{"-", "0.0007268418744505456`"}], ",", 
     RowBox[{"-", "0.0007268428487352541`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "0.010210266560811337`", ",", "0.0008561713132332338`", ",", 
     "0.0008561719972170062`", ",", "0.0008561708777688193`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "0.013039889360576184`"}], ",", 
     RowBox[{"-", "0.0014737750506714532`"}], ",", 
     RowBox[{"-", "0.0014737754901545039`"}], ",", 
     RowBox[{"-", "0.0014737786837502043`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "0.013936187648646485`"}], ",", 
     RowBox[{"-", "0.0023700733387417537`"}], ",", 
     RowBox[{"-", "0.0023700737782248044`"}], ",", 
     RowBox[{"-", "0.0023700769718205048`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "0.019805690704660466`"}], ",", "0.0033265379151489957`", 
     ",", "0.0033265370361828944`", ",", "0.0033265306489914936`"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.7627732606493692`*^9, 3.762773292251108*^9}, {
   3.76277332905138*^9, 3.7627733403677597`*^9}, 3.76277337233559*^9, 
   3.762773425770746*^9, {3.76277370484793*^9, 3.7627737313981743`*^9}, 
   3.7627740189128847`*^9, {3.762774063479789*^9, 3.762774080111012*^9}, 
   3.7627741246420507`*^9, 3.762774182616146*^9, 3.762774256667626*^9, 
   3.762774389352944*^9, 3.7627745960881042`*^9, 3.762774887615808*^9, {
   3.762774931711205*^9, 3.762774947197489*^9}, 3.763229800488722*^9, 
   3.763229945490013*^9, {3.763230069240102*^9, 3.7632300770719757`*^9}, {
   3.763230120956531*^9, 3.763230139321487*^9}, 3.7632301753878*^9, 
   3.763230913042848*^9, 3.763320806265379*^9, 3.763320889161295*^9, 
   3.77134492484385*^9, 3.779384144108588*^9, {3.783022856920787*^9, 
   3.783022863552107*^9}},
 CellLabel->
  "Out[234]=",ExpressionUUID->"a60e188c-2444-48d2-860c-b4d7668b3fdd"],

Cell[BoxData[
 TemplateBox[{GraphicsBox[
    RasterBox[{{0.026976077009222565`, 0.043455229798734035`, 
     0.04345523067770014, 0.04345523706489154}, {0.032845580065236545`, 
     0.04441169437514128, 0.04441169393565823, 0.044411690742062526`}, {
     0.033741878353306845`, 0.045307992663211574`, 0.04530799222372853, 
     0.04530798903013283}, {0.036571501153071694`, 0.0459255964006498, 
     0.045925595716666025`, 0.04592559683611421}, {0.0376287825760837, 
     0.046054925135472206`, 0.046054925839432485`, 0.04605492486514778}, {
     0.03176073283409394, 0.046587776187423977`, 0.04658777536824267, 
     0.0465877806791457}, {0., 0.046264457239618924`, 0.04626445548168672, 
     0.04626444270730392}, {0.027418516015272364`, 0.046652438979060594`, 
     0.046652437591116556`, 0.046652439684849605`}, {0.03438428657839936, 
     0.04658777750090009, 0.046587777950093105`, 0.04658775777107443}}, {{0, 
     0}, {4, 9}}, {0, 0.04678176771388303}], Frame -> Automatic, 
    FrameLabel -> {None, None}, FrameTicks -> {{{{8.5, 
         FormBox["\"no errors\"", TraditionalForm]}, {7.5, 
         FormBox["\"one error\"", TraditionalForm]}, {6.5, 
         FormBox["\"two errors\"", TraditionalForm]}, {5.5, 
         FormBox["\"complex\"", TraditionalForm]}, {4.5, 
         FormBox["\"trisomy\"", TraditionalForm]}, {3.5, 
         FormBox["\"monosomy\"", TraditionalForm]}, {2.5, 
         FormBox["\"double trisomy\"", TraditionalForm]}, {1.5, 
         FormBox["\"double monosomy\"", TraditionalForm]}, {0.5, 
         FormBox["\"trisomy+monosomy\"", TraditionalForm]}}, None}, {
      None, None}}, GridLinesStyle -> Directive[
      GrayLevel[0.5, 0.4]], 
    Method -> {
     "DefaultBoundaryStyle" -> Automatic, 
      "DefaultGraphicsInteraction" -> {
       "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
        "Effects" -> {
         "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
          "Droplines" -> {
           "freeformCursorMode" -> True, 
            "placement" -> {"x" -> "All", "y" -> "None"}}}}, 
      "DefaultPlotStyle" -> Automatic}],FormBox[
    FormBox[
     TemplateBox[{
       FormBox[
        StyleBox[
         StyleBox[
          PaneBox[
           GraphicsBox[{
             RasterBox[CompressedData["
1:eJx1xn1cywkAx/F5WtGtfrJeHXkq8xCpNuRCCktlLr1ynK5hV2uVh+lUzENk
sRUijxVKUhJDW4SZtJDlqYmW2m+JVSixkFaczv1739d9X6/P6/11DFsXHNGP
QqFU/qjPj/79+nF1ExLef/D+r9RR45SZd1F3+1hOyQk01KaMrI5FJRY04YcF
aFFvSK+VE6rvOpM2ofs9SDV9cvTVou5vfIr/LEBDG1PZ27ahkuf1NceWoEXa
8ZFXXVC9Js78tB9KVatTTPXtoPt1aweaAg2Vh8qcU1BJ4Vmv+Ty06FTn47Dp
qD5zLm+7NUo9uL/jePM7kLmbFF9ToaFiZ3rNIVSyeUN+xypUvv62h/VcVL+K
0EwcilLDl4f4mdpAZui51vB7KHdx15bEbFTCYdOy4lH5vAPZ1xei5MwGN90Y
lDp1kvpjTyvIdBEF21SjXMZd46RCVDLcNt4/EZXTV1IjfkfJn2TpO1xR6oDu
CdkDUObfvkol+Rbkdh7k1BajkvYX5KfdqLzZRUiEoaRhU6+LJ0rVVaQFECjz
8RAnwes3ILeCVywuRSWlF9gnj6Dykp6aG2tQ8qJf5PN5qEXBYfPnYSjz5MuU
wR9fg9x0VwfXSlS6f4tsQQ4ql2q8Ijei5Ha7qqRA1EIUxssZi7JiLnWovrWA
3Khv4rqnqIQXQP9yDlUsO5pvK0bJIKOHWwhqEeCu4bijrDkJIVEWKNfzfuvO
hmZQyrTfeuoKqnDm00r3oqSjPLs+HKUO++7WNQNl2XLUQ2xR7qCMYPe3TaC0
b7NxYRmq6GHGR6ej5MdtVIkQtWh7kJ7ri7KMPzvfGo5y9RFK/ScjKH2q4Jjv
o4oHvSQ9FyVvLxQyN6EWqszeX4NQ1uWWtFXjUa5sipP0+ytQmpdYfLoGVZx4
xC6ToYbDw3RkEmqZGhnZ/QfK2nXZbMdClyf02c0aiCbHBzosanwJKtYel62+
ihoi3ngl70MtV0yryotAWUvFPPUslBtY1WEYgibPH57U09oIKmZH0+3LUdKj
JH9KJmrp1m96UAzKGh+kWeOHckdlhaSMRAm3ZzWcthcgf5TQvDMTVdpYOpT6
oQQl16urswHkm2by3PNQZWONODoYtX6yLj+XggrUAzX6iwZQKT/dSl+OErle
tEArVHCw1k16nQSV4r+CyyJRItYqvtsOFYTnp7Pu6EHlYm/l6vUowa4j80aj
gqmxvYbH9aCSQXOyT0AJuwJ20CSUP2BOZEpdHajqrE8pl6JES5zs6zSUr7Ou
mtr0HFRVnO1YexAlrs6lF/igggLSo/F9LahK3xAyNAslkomtwRxUIDqXvadb
B6qi2Oo7BSgR0mD8ewkqCBBRp/dHlZ62zjGKGpCYKOMU8lDBsPnCV9aoalBj
msPNZyDxdVPxb6tRQdsQXepQVKW/YK649xQkHvo5UDagAtVLL08GqpJt4a2v
rgaJLLuk84moIPVSfpMrqkoI0IwwPAEJobF16R5UsCKBluaJlgbau1e+1oKE
tzy471FU4MaJn8lG47zvu71tqgJHj/4eFzoH1VJYykdZj8G4lxG93j2PQEZ5
JluxFNXmPkphFD8ERUl9qo7aoAz+NPrANQ9ALTs6ZIvmPigam5XdzkAZ1CfG
lTsqQW1Lf+dqgwYU3ftFyJ6BMs6uKS45eg/UJueYJ3yqAEXRz7yOL0IZCyyT
aLK7oHbiLM12S1RkFUP7yL8DMt6dDuarb4Pah7XpuhGo6IKVwX9zOcjY5+10
Q6cGtcLYyMlTUNGiAtnJ/WWgP6/EOOb5LTDDcaTQ8kApaHq1y/wu4Cbon9cu
ftJXBWZELKGV3FCCpnE304/FXQeD3jCctk++BmYU7pWFt5SA5lWfPfxPXgGD
XLhql2WXwYz225zBg4tB88VJus5KORgUc4hXLy4CfcK/1CRsvAiamu+2zn97
HsyJOtJrE1oI+rTx6XUPz4CmtVOdc2fngTmmfrNXF50CE1vO5Eexs8HGNt+q
nvGZoE9Hk3mv1WGQ0m7hO7shFSzr8s0Iy08CKQMsa2l169H/2T8eZ3xs
              "], {{
                Rational[-15, 2], (-180)/GoldenRatio}, {
                Rational[15, 2], 180/GoldenRatio}}], {Antialiasing -> False, 
              AbsoluteThickness[0.1], 
              Directive[
               Opacity[0.3], 
               GrayLevel[0]], 
              LineBox[
               NCache[{{
                  Rational[15, 2], (-180)/GoldenRatio}, {
                  Rational[-15, 2], (-180)/GoldenRatio}, {
                  Rational[-15, 2], 180/GoldenRatio}, {
                  Rational[15, 2], 180/GoldenRatio}, {
                  Rational[15, 2], (-180)/GoldenRatio}}, {{
                7.5, -111.24611797498106`}, {-7.5, -111.24611797498106`}, \
{-7.5, 111.24611797498106`}, {7.5, 111.24611797498106`}, {
                7.5, -111.24611797498106`}}]]}, {
              CapForm[None], {}}, 
             StyleBox[{Antialiasing -> False, 
               StyleBox[
                
                LineBox[{{7.500000000000002, -111.24611797498105`}, {
                 7.500000000000002, 111.24611797498106`}}], 
                Directive[
                 AbsoluteThickness[0.2], 
                 Opacity[0.3], 
                 GrayLevel[0]], StripOnInput -> False], 
               StyleBox[
                StyleBox[{{
                   StyleBox[
                    LineBox[{{{7.500000000000002, -64.07966966867215}, 
                    
                    Offset[{4., 0}, {
                    7.500000000000002, -64.07966966867215}]}, {{
                    7.500000000000002, -16.43679259159246}, 
                    
                    Offset[{4., 0}, {
                    7.500000000000002, -16.43679259159246}]}, {{
                    7.500000000000002, 31.206084485487214`}, 
                    
                    Offset[{4., 0}, {7.500000000000002, 
                    31.206084485487214`}]}, {{7.500000000000002, 
                    78.84896156256693}, 
                    
                    Offset[{4., 0}, {7.500000000000002, 
                    78.84896156256693}]}}], 
                    Directive[
                    AbsoluteThickness[0.2], 
                    GrayLevel[0.4]], StripOnInput -> False], 
                   StyleBox[
                    LineBox[{{{7.500000000000002, -102.19397133033591`}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -102.19397133033591`}]}, {{
                    7.500000000000002, -92.66539591491997}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -92.66539591491997}]}, {{
                    7.500000000000002, -83.13682049950403}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -83.13682049950403}]}, {{
                    7.500000000000002, -73.6082450840881}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -73.6082450840881}]}, {{
                    7.500000000000002, -54.55109425325622}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -54.55109425325622}]}, {{
                    7.500000000000002, -45.02251883784029}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -45.02251883784029}]}, {{
                    7.500000000000002, -35.493943422424344`}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -35.493943422424344`}]}, {{
                    7.500000000000002, -25.96536800700842}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -25.96536800700842}]}, {{
                    7.500000000000002, -6.9082171761765405`}, 
                    
                    Offset[{2.5, 0.}, {
                    7.500000000000002, -6.9082171761765405`}]}, {{
                    7.500000000000002, 2.6203582392394167`}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    2.6203582392394167`}]}, {{7.500000000000002, 
                    12.148933654655336`}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    12.148933654655336`}]}, {{7.500000000000002, 
                    21.677509070071274`}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    21.677509070071274`}]}, {{7.500000000000002, 
                    40.734659900903154`}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    40.734659900903154`}]}, {{7.500000000000002, 
                    50.26323531631909}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    50.26323531631909}]}, {{7.500000000000002, 
                    59.79181073173501}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    59.79181073173501}]}, {{7.500000000000002, 
                    69.32038614715097}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    69.32038614715097}]}, {{7.500000000000002, 
                    88.37753697798284}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    88.37753697798284}]}, {{7.500000000000002, 
                    97.90611239339877}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    97.90611239339877}]}, {{7.500000000000002, 
                    107.43468780881469`}, 
                    
                    Offset[{2.5, 0.}, {7.500000000000002, 
                    107.43468780881469`}]}}], 
                    Directive[
                    AbsoluteThickness[0.2], 
                    GrayLevel[0.4], 
                    Opacity[0.3]], StripOnInput -> False]}, 
                  StyleBox[
                   StyleBox[{{
                    StyleBox[{
                    InsetBox[
                    FormBox[
                    TagBox[
                    InterpretationBox["\"0.01\"", 0.01, AutoDelete -> True], 
                    NumberForm[#, {
                    DirectedInfinity[1], 2}]& ], TraditionalForm], 
                    
                    Offset[{7., 0.}, {
                    7.500000000000002, -64.07966966867215}], {-1, 0.}, 
                    Automatic, {1, 0}], 
                    InsetBox[
                    FormBox[
                    TagBox[
                    InterpretationBox["\"0.02\"", 0.02, AutoDelete -> True], 
                    NumberForm[#, {
                    DirectedInfinity[1], 2}]& ], TraditionalForm], 
                    Offset[{7., 0.}, {
                    7.500000000000002, -16.43679259159246}], {-1, 0.}, 
                    Automatic, {1, 0}], 
                    InsetBox[
                    FormBox[
                    TagBox[
                    InterpretationBox["\"0.03\"", 0.03, AutoDelete -> True], 
                    NumberForm[#, {
                    DirectedInfinity[1], 2}]& ], TraditionalForm], 
                    
                    Offset[{7., 0.}, {7.500000000000002, 
                    31.206084485487214`}], {-1, 0.}, Automatic, {1, 0}], 
                    InsetBox[
                    FormBox[
                    TagBox[
                    InterpretationBox["\"0.04\"", 0.04, AutoDelete -> True], 
                    NumberForm[#, {
                    DirectedInfinity[1], 2}]& ], TraditionalForm], 
                    
                    Offset[{7., 0.}, {7.500000000000002, 
                    78.84896156256693}], {-1, 0.}, Automatic, {1, 0}]}, 
                    Directive[
                    AbsoluteThickness[0.2], 
                    GrayLevel[0.4]], {
                    Directive[
                    Opacity[1]], 
                    Directive[
                    Opacity[1]]}, StripOnInput -> False], 
                    
                    StyleBox[{{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \
{}, {}, {}, {}, {}, {}, {}}, 
                    Directive[
                    AbsoluteThickness[0.2], 
                    GrayLevel[0.4], 
                    Opacity[0.3]], {
                    Directive[
                    Opacity[1]], 
                    Directive[
                    Opacity[1]]}, StripOnInput -> False]}, {}}, {
                    Directive[
                    Opacity[1]], 
                    Directive[
                    Opacity[1]]}, StripOnInput -> False], "GraphicsLabel", 
                   StripOnInput -> False]}, "GraphicsTicks", StripOnInput -> 
                 False], {
                 Directive[
                  AbsoluteThickness[0.2], 
                  GrayLevel[0.4]], 
                 Directive[
                  AbsoluteThickness[0.2], 
                  GrayLevel[0.4], 
                  Opacity[0.3]]}, StripOnInput -> False]}, "GraphicsAxes", 
              StripOnInput -> False]}, PlotRangePadding -> Scaled[0.02], 
            PlotRange -> All, Frame -> True, 
            FrameTicks -> {{False, False}, {True, False}}, FrameStyle -> 
            Opacity[0], FrameTicksStyle -> Opacity[0], ImageSize -> 
            NCache[{Automatic, 360/GoldenRatio}, {
              Automatic, 222.49223594996212`}], BaseStyle -> {}], Alignment -> 
           Left, AppearanceElements -> None, ImageMargins -> {{5, 5}, {5, 5}},
            ImageSizeAction -> "ResizeToFit"], LineIndent -> 0, StripOnInput -> 
          False], {FontFamily -> "Arial"}, Background -> Automatic, 
         StripOnInput -> False], TraditionalForm]}, "BarLegend", 
      DisplayFunction -> (#& ), 
      InterpretationFunction :> (RowBox[{"BarLegend", "[", 
         RowBox[{
           RowBox[{"{", 
             RowBox[{
               RowBox[{
                 RowBox[{"GrayLevel", "[", 
                   RowBox[{"Rescale", "[", 
                    RowBox[{"#1", ",", 
                    RowBox[{"{", 
                    
                    RowBox[{
                    "0.00012932802903342644`", ",", "0.04678176771388303`"}], 
                    "}"}], ",", 
                    RowBox[{"{", 
                    RowBox[{"0.9972355035871155`", ",", "0.`"}], "}"}]}], 
                    "]"}], "]"}], "&"}], ",", 
               RowBox[{"{", 
                 
                 RowBox[{
                  "0.00012932802903342644`", ",", "0.04678176771388303`"}], 
                 "}"}]}], "}"}], ",", 
           RowBox[{"LabelStyle", "\[Rule]", 
             RowBox[{"{", "}"}]}], ",", 
           RowBox[{"LegendLayout", "\[Rule]", "\"Column\""}], ",", 
           RowBox[{"LegendMarkerSize", "\[Rule]", 
             FractionBox["360", "GoldenRatio"]}], ",", 
           RowBox[{"Charting`TickSide", "\[Rule]", "Right"}], ",", 
           RowBox[{"ColorFunctionScaling", "\[Rule]", "False"}]}], "]"}]& )], 
     TraditionalForm], TraditionalForm]},
  "Legended",
  DisplayFunction->(GridBox[{{
      TagBox[
       ItemBox[
        PaneBox[
         TagBox[#, "SkipImageSizeLevel"], Alignment -> {Center, Baseline}, 
         BaselinePosition -> Baseline], DefaultBaseStyle -> "Labeled"], 
       "SkipImageSizeLevel"], 
      ItemBox[#2, DefaultBaseStyle -> "LabeledLabel"]}}, 
    GridBoxAlignment -> {"Columns" -> {{Center}}, "Rows" -> {{Center}}}, 
    AutoDelete -> False, GridBoxItemSize -> Automatic, 
    BaselinePosition -> {1, 1}]& ),
  Editable->True,
  InterpretationFunction->(RowBox[{"Legended", "[", 
     RowBox[{#, ",", 
       RowBox[{"Placed", "[", 
         RowBox[{#2, ",", "After"}], "]"}]}], "]"}]& )]], "Output",
 CellChangeTimes->{{3.7627732606493692`*^9, 3.762773292251108*^9}, {
   3.76277332905138*^9, 3.7627733403677597`*^9}, 3.76277337233559*^9, 
   3.762773425770746*^9, {3.76277370484793*^9, 3.7627737313981743`*^9}, 
   3.7627740189128847`*^9, {3.762774063479789*^9, 3.762774080111012*^9}, 
   3.7627741246420507`*^9, 3.762774182616146*^9, 3.762774256667626*^9, 
   3.762774389352944*^9, 3.7627745960881042`*^9, 3.762774887615808*^9, {
   3.762774931711205*^9, 3.762774947197489*^9}, 3.763229800488722*^9, 
   3.763229945490013*^9, {3.763230069240102*^9, 3.7632300770719757`*^9}, {
   3.763230120956531*^9, 3.763230139321487*^9}, 3.7632301753878*^9, 
   3.763230913042848*^9, 3.763320806265379*^9, 3.763320889161295*^9, 
   3.77134492484385*^9, 3.779384144108588*^9, {3.783022856920787*^9, 
   3.783022864215906*^9}},
 CellLabel->
  "Out[236]=",ExpressionUUID->"175e9def-13f7-4f43-8c80-4b7c51fdbd97"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Export", "[", 
  RowBox[{
  "\"\<Revision2_day5_model_selection_analysis_rawDataPerIndiv.svg\>\"", ",", 
   "plt"}], "]"}]], "Input",
 CellChangeTimes->{{3.762774949303238*^9, 3.762774955919444*^9}, {
   3.7627749876857147`*^9, 3.762775007896564*^9}, {3.763320895409718*^9, 
   3.763320899719462*^9}, {3.771344940003173*^9, 3.771344941412836*^9}, {
   3.779384154598978*^9, 3.7793841621326933`*^9}, 3.783022700267961*^9},
 CellLabel->
  "In[237]:=",ExpressionUUID->"2e982987-8ebb-49f2-9da9-3b6e5ec3e665"],

Cell[BoxData["\<\"Revision2_day5_model_selection_analysis_rawDataPerIndiv.svg\
\"\>"], "Output",
 CellChangeTimes->{3.7627750106601257`*^9, 3.763230214876788*^9, 
  3.7632309195315943`*^9, 3.7633209222131023`*^9, 3.771344944219533*^9, 
  3.779384165975366*^9, 3.783022869193863*^9},
 CellLabel->
  "Out[237]=",ExpressionUUID->"36a5a80c-a089-4d80-87b5-50b69f50459f"]
}, Open  ]]
},
WindowSize->{1680, 1005},
WindowMargins->{{20, Automatic}, {Automatic, 3}},
FrontEndVersion->"12.0 for Mac OS X x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 156, 3, 30, "Input",ExpressionUUID->"88f33434-d7de-4e97-a359-8fae94630853"],
Cell[717, 25, 279, 6, 30, "Input",ExpressionUUID->"1e7522db-f960-4479-b4f0-5ba4f7b1ba59"],
Cell[CellGroupData[{
Cell[1021, 35, 1019, 23, 73, "Input",ExpressionUUID->"db33ecc3-73dd-45e6-ba43-07a155814ec3"],
Cell[2043, 60, 678, 12, 34, "Output",ExpressionUUID->"2275eacb-85e4-4cc6-a5a2-2a8ba105b721"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2758, 77, 405, 8, 30, "Input",ExpressionUUID->"381ad6e2-9548-48ba-9741-c9f12196d5d4"],
Cell[3166, 87, 662, 12, 34, "Output",ExpressionUUID->"b37d9c85-3af1-4e3d-9128-038a71680567"]
}, Open  ]],
Cell[3843, 102, 1780, 37, 94, "Input",ExpressionUUID->"138aa53b-110b-47b4-927d-6ae585ee10f1"],
Cell[5626, 141, 648, 17, 73, "Input",ExpressionUUID->"1dbecdc4-4c27-4b65-b766-c979ffc9aae1"],
Cell[CellGroupData[{
Cell[6299, 162, 3350, 81, 157, "Input",ExpressionUUID->"394fb833-604e-456c-8a85-452e141656f3"],
Cell[9652, 245, 2804, 61, 77, "Output",ExpressionUUID->"a60e188c-2444-48d2-860c-b4d7668b3fdd"],
Cell[12459, 308, 16971, 350, 451, "Output",ExpressionUUID->"175e9def-13f7-4f43-8c80-4b7c51fdbd97"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29467, 663, 528, 10, 30, "Input",ExpressionUUID->"2e982987-8ebb-49f2-9da9-3b6e5ec3e665"],
Cell[29998, 675, 365, 6, 34, "Output",ExpressionUUID->"36a5a80c-a089-4d80-87b5-50b69f50459f"]
}, Open  ]]
}
]
*)

