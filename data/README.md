*******************
* To generate: 

- TableS1_day5_parsed_data.RData

Use codes

codes/parsing_data.R
codes/1.process_data.R

*******************
* To generate: 

- day5_data_rawPerIndv_grouped.csv 
- day5_proportions_rawPerIndv.csv
- day5_data_GroupEggsCases.csv
- day5_data_rawPerIndv_GroupEggsCases_ver4.csv
- day5_data_rawPerIndv_groupedByAge_2920rows.csv

Use code

codes/2.1.Figure2-export_rawdata.R

*******************
* To generate parameters stored in: 

- Revision_Model2-Normal_23q-MINDJ_23qstar-MIPSSC_23qstar-day5-BESTFIT.csv

Use code

figures/Figure2BCD-FigureS1BtoF.nb

*******************
* To generate: 

- day5_ErrorsByAge.csv
- day5_perChromErrors.csv

Use code

codes/2.6.Suppl-info.R