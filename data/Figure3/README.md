
Run simulations:

sbatch --array=1-186 model_simulations_parallel.sh

Concatanate the files in alphanumeric order:

cat $(ls -1v simsresults/100000simulations_*) > file.txt

