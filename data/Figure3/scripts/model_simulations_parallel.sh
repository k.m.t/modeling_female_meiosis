#!/bin/bash

#SBATCH --partition=genetics_1       # Partition (job queue)
#SBATCH --requeue                    # Return job to the queue if preempted
#SBATCH --job-name=calcpVals         # Assign an short name to your job
#SBATCH --nodes=1                    # Number of nodes you require
#SBATCH --ntasks=1                   # Total # of tasks across all nodes
#SBATCH --cpus-per-task=1            # Cores per task (>1 if multithread tasks)
#SBATCH --mem=2000                   # Real memory (RAM) required (MB)
#SBATCH --time=10:00:00              # Total run time limit (HH:MM:SS)
#SBATCH --output=logs/calcpVals.%N.%j.out     # STDOUT output file
#SBATCH --error=logs/calcpVals.%N.%j.err      # STDERR output file (optional)
#SBATCH --export=ALL                 # Export you current env to the job env

cd /projects/f_jx76_1/Katarzyna_projects/MeiosisModeling/20191006_SimulationsForRevision

module load Mathematica/11.3

# rm -rf $PWD/mathematica.kt464.tmp
# mkdir $PWD/mathematica.kt464.tmp
export TemporaryDirectory=$PWD/mathematica.kt464.tmp

file=data/day5_data_GroupEggsCases.csv

line=$(head -n $SLURM_ARRAY_TASK_ID $file | tail -1)

age=$(echo $line | tr ',' '\t' | cut -f1)
oocytes=$(echo $line | tr ',' '\t' | cut -f2)

echo $age
echo $oocytes

srun math -noprompt -script scripts/modelsims.m $age $oocytes 100000

