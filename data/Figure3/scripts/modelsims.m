
Print[$CommandLine];

vars = ToExpression[Rest[$CommandLine]];

fittedparamvalsperage = Import["data/Revision_Model2-Normal_23q-MINDJ_23qstar-MIPSSC_23qstar-day5-BESTFIT.csv", "CSV"];
fittedparamvalsperage = DeleteCases[fittedparamvalsperage, {__String}, {-2}];

embpointsAnplRateSim = {};

roundto = 0.01;

AGE = vars[[4]];
oocytes = vars[[5]];

simulations = vars[[6]];

minAge = 27;

dVector = fittedparamvalsperage[[AGE - (minAge-1)]][[2]]; 
pVector = fittedparamvalsperage[[AGE - (minAge-1)]][[3]];
cVector = fittedparamvalsperage[[AGE - (minAge-1)]][[4]];
qVector = fittedparamvalsperage[[AGE - (minAge-1)]][[5]];
qstarVector = fittedparamvalsperage[[AGE - (minAge-1)]][[6]];

prcAneuploidyRate = {};

For[j = 1, j <= simulations, j++,
 
 w = {1 - dVector - pVector - cVector, dVector, pVector, cVector};
  
 meiosis1 = RandomChoice[w -> {1, 2, 3, 4}, oocytes]; 
 
 meiosis1noError = RandomChoice[{1 - 23*qVector, 23*qVector} -> {1, 2}, Count[meiosis1, 1]];
 g1 = RandomChoice[{Euploid}, Count[meiosis1noError, 1]];
 g2 = RandomChoice[{Monosomy, Trisomy}, Count[meiosis1noError, 2]];
  
 meiosis1ND = RandomChoice[{minustwo, plustwo}, Count[meiosis1, 2]];
 
 meiosis1NDminustwo = RandomChoice[{1 - 22*qstarVector, 22*qstarVector } -> {1, 2}, Count[meiosis1ND, minustwo]];
 g3 = RandomChoice[{Monosomy}, Count[meiosis1NDminustwo, 1]];
 g4 = RandomChoice[{DblMonosomy, MonosomyTrisomy}, Count[meiosis1NDminustwo, 2]];
 
 meiosis1NDplustwo = RandomChoice[{1 - 23*qstarVector, qstarVector, 22*qstarVector } -> {1, 2, 3}, Count[meiosis1ND, plustwo]];
 g5 = RandomChoice[{Trisomy}, Count[meiosis1NDplustwo, 1]];
 g6 = RandomChoice[{Euploid, ExtraChromPair}, Count[meiosis1NDplustwo, 2]];
 g7 = RandomChoice[{DblTrisomy, MonosomyTrisomy}, Count[meiosis1NDplustwo, 3]];
 
 meiosis1PSSC = RandomChoice[{minusone, plusone}, Count[meiosis1, 3]];
 
 meiosis1PSSCminusone = RandomChoice[{1 - 22*qstarVector, 22*qstarVector } -> {1, 2}, Count[meiosis1PSSC, minusone]];
 g8 = RandomChoice[{Euploid, Monosomy}, Count[meiosis1PSSCminusone, 1]];
 g9 = RandomChoice[{DblMonosomy, Trisomy, Monosomy, MonosomyTrisomy}, Count[meiosis1PSSCminusone, 2]];
 
 meiosis1PSSCplusone = RandomChoice[{1 - 23*qstarVector, qstarVector, 22*qstarVector } -> {1, 2, 3}, Count[meiosis1PSSC, plusone]];
 g10 = RandomChoice[{Euploid, Trisomy}, Count[meiosis1PSSCplusone, 1]];
 g11 = RandomChoice[{Euploid, Trisomy, Monosomy, ExtraChromPair}, Count[meiosis1PSSCplusone, 2]];
 g12 = RandomChoice[{Trisomy, MonosomyTrisomy, Monosomy, DblTrisomy}, Count[meiosis1PSSCplusone, 3]];
 
 g13 = RandomChoice[{ComplexCases}, Count[meiosis1, 4]];
 
 euploidEmbryos = Count[Flatten[{g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13}], Euploid];
 aneuploidEmbryos = Count[Flatten[{g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13}], Trisomy | Monosomy | DblMonosomy | DblTrisomy | MonosomyTrisomy | ExtraChromPair | ComplexCases];
 
 aneuploidyRate = Round[N[AneuploidyRate = aneuploidEmbryos/(aneuploidEmbryos + euploidEmbryos)], roundto];
 AppendTo[prcAneuploidyRate, aneuploidyRate];
 
 embpointsAnplRateSim = AppendTo[embpointsAnplRateSim, aneuploidyRate]

 ]

distrfile = "simresults/" <> ToString[simulations] <> "simulations_" <> ToString[AGE] <> "y_" <> ToString[oocytes] <> "oocytes.csv";
Export[distrfile, Join[ConstantArray[{AGE, oocytes}, oocytes + 1], Sort[Tally[embpointsAnplRateSim]], 2] ]

