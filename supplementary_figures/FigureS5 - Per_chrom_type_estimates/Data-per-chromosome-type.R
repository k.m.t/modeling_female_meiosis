
load("../../data/TableS1_day5_parsed_data.RData")

#********************************************

library(lubridate)
library(magrittr)
library(dplyr)

#********************************************

# https://stackoverflow.com/questions/6451152/how-to-catch-integer0
is.integer0 <- function(x)
{
  is.integer(x) && length(x) == 0L
}

#********************************************
df = df.merge[ indx, 7:29]

sapply(apply(as.matrix(df[1,]), 1, function(x){which(x == "H010")}), function(x)x[1])
sapply(apply(as.matrix(df[1,]), 1, function(x){which(x == "H210")}), function(x)x[1])

indx = which(df.merge$chroms_affected == 1)

df.merge$bytrisChrom = 0
df.merge$bymonosChrom = 0

for(i in indx){
  rowCheck = apply(df.merge[i,], 1, function(x){which(x == "H010" | x == "H001")})
  if(is.integer0(rowCheck)){
    df.merge$bytrisChrom[i] = (apply(df.merge[i,], 1, function(x){which(x == "H210" | x == "H201")}) - 6)
  } else {
    df.merge$bymonosChrom[i] = (rowCheck - 6)
  }
}

head(df.merge[,c("bytrisChrom", "bymonosChrom")], 25)
head(df.merge[indx[1:5],c("bytrisChrom", "bymonosChrom")])

acrocentric = c(13, 14, 15, 21, 22)
metacentric = c(1, 3, 16, 19,20)
submetacentric = c(2, 4:12, 17:18)
sexchr = 23

df.merge$chrType = "NA"
df.merge$chrType[which(df.merge$bytrisChrom %in% acrocentric)] = "acrocentric"
df.merge$chrType[which(df.merge$bytrisChrom %in% metacentric)] = "metacentric"
df.merge$chrType[which(df.merge$bytrisChrom %in% submetacentric)] = "submetacentric"
df.merge$chrType[which(df.merge$bytrisChrom %in% sexchr)] = "sexchr"

df.merge$chrType[which(df.merge$bymonosChrom %in% acrocentric)] = "acrocentric"
df.merge$chrType[which(df.merge$bymonosChrom %in% metacentric)] = "metacentric"
df.merge$chrType[which(df.merge$bymonosChrom %in% submetacentric)] = "submetacentric"
df.merge$chrType[which(df.merge$bymonosChrom %in% sexchr)] = "sexchr"

table(df.merge$chrType)

#********************************************

df.aggr <- df.merge %>%
  dplyr::select(case, maternal_age, embryoid, chroms_affected, chroms_trisomic, chroms_monosomic, chrType) %>%
  group_by(case) %>%
  summarise("maternal_age" = unique(round(maternal_age)),
            "sum_eggs" = length(embryoid),
            
            "noError" = length(which(chroms_affected == 0)),
           
            "acrocentricError" = length(which(chroms_affected == 1 & chrType == "acrocentric")),
            "metacentric" = length(which(chroms_affected == 1 & chrType == "metacentric")),
            "submetacentric" = length(which(chroms_affected == 1 & chrType == "submetacentric")),
            "sexchr" = length(which(chroms_affected == 1 & chrType == "sexchr")),
            
            "twoErrors" = length(which(chroms_affected == 2)),
            "complex" = length(which(chroms_affected > 2)),
               )

#********************************************
# Pull younger and older patients 
#********************************************

df.aggr$group = df.aggr$maternal_age

# Group '27-28'
df.aggr$group[which(df.aggr$group==27 | df.aggr$group==28)] <- 28   # Remember this is Group '27-28'

# Group '20-26'
df.aggr$group[which(df.aggr$group <= 26)] <- 27                     # Remember this is Group '20-26'

# Group '44-48'
df.aggr$group[which(df.aggr$group >= 44)] <- 44                     # Remember this is Group '44-48'

#********************************************
# Raw data we use for 'age-specific' parameter estimation task.
#********************************************

options(scipen = 999)                                               # To force scientific notation, not exponential, e.g., 1e-4

# To get raw proportions, normalize raw counts by the total number of embryos

group.patients = function(df){
  df.processed <- df %>%
    dplyr::select(group, case, sum_eggs, noError, 
           acrocentricError, metacentric, submetacentric, sexchr, twoErrors, complex) %>%
    group_by(group) %>%
    summarise("cases" = length(case),
              "sum_eggs" = sum(sum_eggs),
              "eggPercase" = sum(sum_eggs)/length(case),
              
              "noError_prop" = sum(noError)/sum(sum_eggs),
              
              "acrocentricError_prop" = sum(acrocentricError)/sum(sum_eggs),
              "metacentric_prop" = sum(metacentric)/sum(sum_eggs),
              "submetacentric_prop" = sum(submetacentric)/sum(sum_eggs),
              "sexchr_prop" = sum(sexchr)/sum(sum_eggs),
              
              "twoErrors_prop" = sum(twoErrors)/sum(sum_eggs),
              "complex_prop" = sum(complex)/sum(sum_eggs)
    )
  return(df.processed)
}

data = group.patients(df.aggr)  

# write.table(data, "day5_data_rawPerIndv_ChromSpecific_grouped.csv", quote = F, row.names = F, col.names = T, sep = ",")
