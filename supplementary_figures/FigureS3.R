
# K.M.Tyc 
# Last modified: November 18, 2019

# Input: "data/TableS1_day5_parsed_data.RData"
# See "data/README" for details on the input file

#*******************************************
# Fig. S3A
#*******************************************

countChromosomes = read.csv("data/day5_perChromErrors.csv", sep = ",", header = T)

data = countChromosomes[, c("chrom", "singlTrisomy", "singlMonosomy")]
colnames(data) = c("chrom", "single trisomy", "single monosomy")

data.m = reshape2::melt(data, "chrom")

ggplot(data.m, aes(chrom, value, width=.5)) +   
  geom_bar(aes(fill = variable), position = "dodge", stat="identity") + 
  scale_x_discrete() + 
  geom_text(aes(x=c(1:23,1:23), y=-1, label=c(1:22,"X/Y", 1:22,"X/Y")), vjust=1.2, size=3) + 
  labs(x="Affected chromosome", y="Total number of embryos") + theme_bw()

#*******************************************
# Fig. S3B
#*******************************************

summary_te = read.csv("Data/day5_ErrorsByAge.csv", sep = ",", header = T)

data = summary_te[, c("age", "single_trisomies", "single_monosomies")]
colnames(data) = c("age", "single trisomy", "single monosomy")

data.m = reshape2::melt(data, "age")

ggplot(data.m, aes(age, value, width=.4)) +   
  geom_bar(aes(fill = variable), position = "dodge", stat="identity") + 
  annotate("text", x = summary_te$age, y = 155, label = c(summary_te$totalEggs), 
           angle = 90, size = 3) +
  annotate("text", x = summary_te$age, y = 175, label = c(summary_te$No.patients), 
           angle = 90, size = 3) +
  labs(x="Maternal age (years)", y="Number of embryos") + theme_bw()

#********************************************
# Figure S3C
#********************************************

load("data/TableS1_day5_parsed_data.RData")

df.moreErr = df.merge[ df.merge$chroms_affected > 2, ]
df.moreErr$maternal_age = round(df.moreErr$maternal_age)

library(dplyr) 

nbPatients = df.moreErr %>%
  select("case","maternal_age","chroms_affected") %>%
  group_by(chroms_affected) %>%
  summarize(counts = n())

nbPatients$percent_total = nbPatients$counts/dim(df.merge)[1]*100

library(ggplot2)

ggplot(nbPatients, aes(x = chroms_affected, y = percent_total)) + 
  geom_bar(position="stack", stat="identity") +
  annotate("text", x = 3:23, y = 3, label = c(nbPatients$counts[1:9], 0, nbPatients$counts[10:20]), angle = 45) +
  scale_x_discrete(name ="Number of affected chromosomes", limits = 3:23) +
  scale_y_continuous(name = "Percent embryos")
